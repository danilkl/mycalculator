//
//  SinOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "SinOperation.h"

@implementation SinOperation

-(double)perform:(double)value{
    return sin(value);
}

@end
