//
//  DivOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "DivOperation.h"

@implementation DivOperation

-(double)performWithSecondArgument:(double)secondArgument{
    return self.firstArgument / secondArgument;
}

@end
