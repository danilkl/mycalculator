//
//  ViewController.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.model = [[CalculatorModel alloc] init];
}


- (IBAction)pressDigit:(UIButton *)sender {
    _prevOperation = sender.currentTitle;
    if(self.numberInputing){
        if(![sender.currentTitle isEqualToString:@","] || ![self.numbericLabel.text containsString:@","]){
            self.numbericLabel.text = [self.numbericLabel.text stringByAppendingString:sender.currentTitle];
        }
    }
    else{
        if ([sender.currentTitle isEqualToString:@","]){
            self.numbericLabel.text = [NSString stringWithFormat:@"0,"];
        }
        else{
            self.numbericLabel.text = sender.currentTitle;
        }
        self.numberInputing = YES;
    }
}

- (IBAction)performOperation:(UIButton *)sender {
    self.numberInputing = NO;
    NSString *temp = self.numbericLabel.text;
    temp = [temp stringByReplacingOccurrencesOfString:@"," withString:@"."];
    double value = temp.doubleValue;
    NSString *operationString = sender.currentTitle;
    
    
    if(sender.currentTitle != _prevOperation || (![sender.currentTitle  isEqual: @"-"] && ![sender.currentTitle  isEqual: @"+"] && ![sender.currentTitle  isEqual: @"÷"] && ![sender.currentTitle  isEqual: @"x"] && ![sender.currentTitle  isEqual: @"="]))
    {
        double result = [self.model performOperation:operationString withValue:value];
        temp = [@(result) stringValue];
        NSLog(@"%@", temp);
        printf("\n%f", result);
        temp = [temp stringByReplacingOccurrencesOfString:@"." withString:@","];
        self.numbericLabel.text = temp;
        _prevOperation = operationString;
    }
    
}


@end


