//
//  PercentOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "PercentOperation.h"

@implementation PercentOperation

-(double)perform:(double)value{
    return value / 100.0;
}

@end
