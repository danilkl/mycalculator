//
//  CosOperation.h
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "UnaryOperation.h"

@interface CosOperation : UnaryOperation

@end
