//
//  SqrtOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "SqrtOperation.h"

@implementation SqrtOperation

-(double)perform:(double)value{
    return sqrt(value);
}

@end
