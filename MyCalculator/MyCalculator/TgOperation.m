//
//  TgOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//


#import "TgOperation.h"

@implementation TgOperation

-(double)perform:(double)value {
    return tan(value);
}

@end
