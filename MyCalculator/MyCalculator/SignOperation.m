//
//  SignOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "SignOperation.h"

@implementation SignOperation

-(double)perform:(double)value {
    return -value;
}

@end
