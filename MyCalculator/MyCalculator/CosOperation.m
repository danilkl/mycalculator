//
//  CosOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "CosOperation.h"

@implementation CosOperation

-(double)perform:(double)value {
    return cos(value);
}

@end
