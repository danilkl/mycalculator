//
//  CalculatorModel.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "CalculatorModel.h"
#import "SumOperation.h"
#import "EqualOperation.h"
#import "DeleteOperation.h"
#import "SubOperation.h"
#import "SinOperation.h"
#import "CosOperation.h"
#import "TgOperation.h"
#import "LnOperation.h"
#import "PercentOperation.h"
#import "SignOperation.h"
#import "DivOperation.h"
#import "SqrtOperation.h"
#import "MulOperation.h"

@implementation CalculatorModel

-(instancetype) init{
    self = [super init];
    if(self){
        _operations = @{
                        @"+" : [[SumOperation alloc]init],
                        @"-" : [[SubOperation alloc]init],
                        @"=" : [[EqualOperation alloc]init],
                        @"x" : [[MulOperation alloc]init],
                        @"sin" : [[SinOperation alloc]init],
                        @"cos" : [[CosOperation alloc]init],
                        @"tg" : [[TgOperation alloc]init],
                        @"ln" : [[LnOperation alloc]init],
                        @"%" : [[PercentOperation alloc]init],
                        @"√" : [[SqrtOperation alloc]init],
                        @"±" : [[SignOperation alloc]init],
                        @"÷" : [[DivOperation alloc]init],
                        @"C" :[[DeleteOperation alloc]init],
                        };
    }
    return self;
}

-(id)operationForOperationString:(NSString *)operationString{
    id operation = [self.operations objectForKey:operationString];
    if(!operation){
        operation =[[UnaryOperation alloc]init];
    }
    return operation;
}

-(double)performOperation:(NSString *)operationString withValue:(double)value{
    id operation = [self operationForOperationString:operationString];
    
    if([operation isKindOfClass:[EqualOperation class]]){
        if(self.currentBinaryOperation){
            value = [self.currentBinaryOperation performWithSecondArgument:value];
        }
    }
    
    if([operation isKindOfClass:[DeleteOperation class]]){
        self.currentBinaryOperation = nil;
        return [operation perform:value];
    }
    
    if([operation isKindOfClass:[UnaryOperation class]]){
        return [operation perform:value];
    }
    
    
    if([operation isKindOfClass:[BinaryOperation class]]){
        self.currentBinaryOperation = operation;
        self.currentBinaryOperation.firstArgument = value;
        return value;
    }
    
    return DBL_MAX;
}




@end
