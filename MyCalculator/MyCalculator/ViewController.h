//
//  ViewController.h
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorModel.h"

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *numbericLabel;

@property (nonatomic, assign, getter=isNumberInputting) BOOL numberInputing;
@property(nonatomic, strong) CalculatorModel *model;

@property (nonatomic, strong) NSString *prevOperation;
@end

