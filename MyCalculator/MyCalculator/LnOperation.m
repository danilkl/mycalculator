//
//  LnOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "LnOperation.h"

@implementation LnOperation

-(double)perform:(double)value{
    return log(value);
    
}

@end
