//
//  UnaryOperation.h
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface UnaryOperation : NSObject

-(double)perform:(double)value;

@end

