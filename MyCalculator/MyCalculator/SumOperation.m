//
//  SumOperation.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import "SumOperation.h"

@implementation SumOperation

-(double)performWithSecondArgument:(double)secondArgument{
    return self.firstArgument + secondArgument;
}

@end
