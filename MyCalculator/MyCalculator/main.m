//
//  main.m
//  MyCalculator
//
//  Created by Danil on 11.11.16.
//  Copyright © 2016 DanilKleshchin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
	
